use std::fmt::Display;
use std::fs;
use std::io::{BufRead, BufReader};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct Location {
    x: u32,
    y: u32,
}

impl Location {
    pub fn new(x: u32, y: u32) -> Self {
        Self { x, y }
    }
}

impl Display for Location {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{},{}", self.x, self.y)
    }
}

struct LineIter<'l> {
    line: &'l Line,
    current: Location,
    first_call: bool,
}

impl<'l> Iterator for LineIter<'l> {
    type Item = Location;

    fn next(&mut self) -> Option<Self::Item> {
        if self.first_call {
            self.first_call = false;
            return Some(self.current.clone());
        }
        if self.current == self.line.end {
            return None;
        }
        if self.current.x < self.line.end.x {
            self.current.x += 1;
        }
        else if self.current.x > self.line.end.x {
            self.current.x -= 1;
        }
        if self.current.y < self.line.end.y {
            self.current.y += 1;
        }
        else if self.current.y > self.line.end.y {
            self.current.y -= 1;
        }
        Some(self.current.clone())
    }
}

#[derive(Debug)]
struct Line {
    start: Location,
    end: Location,
}

impl Line {
    pub fn iter(&self) -> LineIter {
        LineIter {
            line: self,
            current: self.start.clone(),
            first_call: true,
        }
    }
}

impl Display for Line {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} -> {}", self.start, self.end)
    }
}

pub fn run() {
    let lines = BufReader::new(fs::File::open("res/input.txt").unwrap())
        .lines()
        .map(|line| line.unwrap())
        .map(|line| {
            let parts = line.split_once("->").unwrap();
            let start = parts.0.trim().split_once(',').unwrap();
            let start = (
                start.0.parse::<u32>().unwrap(),
                start.1.parse::<u32>().unwrap(),
            );
            let end = parts.1.trim().split_once(',').unwrap();
            let end = (end.0.parse::<u32>().unwrap(), end.1.parse::<u32>().unwrap());
            Line {
                start: Location::new(start.0, start.1),
                end: Location::new(end.0, end.1),
            }
        })
        .collect::<Vec<_>>();

    const SIZE: usize = 1000;
    let mut grid = vec!(vec!(0; SIZE); SIZE);

    for line in lines {
        for point in line.iter() {
            grid[point.y as usize][point.x as usize] += 1;
        }
    }

    let mut danger_zones = 0;
    for x in 0..SIZE {
        for y in 0..SIZE {
            if grid[x][y] >= 2 {
                danger_zones += 1;
            }
        }
    }

    println!("Part2: Amount of dangerous zones looking at H/V and diagonal lines is: {}", danger_zones);
}
