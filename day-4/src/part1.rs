use itermore::IterMore;
use std::fmt::Display;
use std::fs;
use std::io::{BufRead, BufReader};

#[derive(Debug)]
struct BoardNumber {
    number: u32,
    checked: bool,
}

impl BoardNumber {
    pub fn new(number: u32) -> Self {
        Self {
            number,
            checked: false,
        }
    }
}

impl Display for BoardNumber {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{} {}",
            self.number,
            if self.checked { 'X' } else { '_' }
        )
    }
}

#[derive(Debug)]
struct Win {
    score: u32,
}

#[derive(Debug)]
struct Board {
    numbers: [[BoardNumber; 5]; 5],
    last_checked_number: Option<u32>,
    won: Option<Win>,
}

impl Board {
    pub fn from_str_representation(lines: [Vec<u32>; 5]) -> Self {
        /*println!(
            "{}",
            lines
                .clone()
                .map(|line| line
                    .iter()
                    .map(|e| e.to_string())
                    .collect::<Vec<_>>()
                    .join(" "))
                .join("\n")
        );*/
        fn row(input: &Vec<u32>) -> [BoardNumber; 5] {
            [
                BoardNumber::new(input.get(0).unwrap().clone()),
                BoardNumber::new(input.get(1).unwrap().clone()),
                BoardNumber::new(input.get(2).unwrap().clone()),
                BoardNumber::new(input.get(3).unwrap().clone()),
                BoardNumber::new(input.get(4).unwrap().clone()),
            ]
        }
        let numbers = [
            row(&lines[0]),
            row(&lines[1]),
            row(&lines[2]),
            row(&lines[3]),
            row(&lines[4]),
        ];
        Self {
            numbers,
            last_checked_number: None,
            won: None,
        }
    }

    pub fn check_number(&mut self, num: u32) {
        for (row, line) in self.numbers.iter_mut().enumerate() {
            for (col, number) in line.iter_mut().enumerate() {
                if number.number == num {
                    number.checked = true;
                    self.last_checked_number = Some(num);
                    self.update_won_state(row, col);
                    return;
                }
            }
        }
    }

    fn update_won_state(&mut self, row: usize, col: usize) {
        // Is entire row now checked?
        if self.is_row_checked(row) || self.is_col_checked(col) {
            self.won = Some(Win {
                score: self.compute_winning_score(),
            });
        }
    } 

    fn is_row_checked(&self, row: usize) -> bool {
        for number in &self.numbers[row] {
            if !number.checked {
                return false;
            }
        }
        true
    }

    fn is_col_checked(&self, col: usize) -> bool {
        for line in &self.numbers {
            let number = &line[col];
            if !number.checked {
                return false;
            }
        }
        true
    }

    fn compute_winning_score(&self) -> u32 {
        self.sum_unchecked() * self.last_checked_number.expect("No last_checked_number...")
    }

    fn sum_unchecked(&self) -> u32 {
        let mut sum = 0;
        for row in &self.numbers {
            for number in row {
                if !number.checked {
                    sum += number.number;
                }
            }
        }
        sum
    }
}

impl Display for Board {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Board: \n{}",
            self.numbers
                .iter()
                .map(|line| line
                    .iter()
                    .map(|num| num.to_string())
                    .collect::<Vec<String>>()
                    .join(",   "))
                .collect::<Vec<String>>()
                .join("\n")
        )
    }
}

pub fn run() {
    let random_numbers = BufReader::new(fs::File::open("res/input_random.txt").unwrap())
        .lines()
        .map(|line| line.unwrap())
        .flat_map(|line| {
            line.split(",")
                .map(|elem| elem.parse::<u32>().unwrap())
                .collect::<Vec<_>>()
        });

    let mut boards = BufReader::new(fs::File::open("res/input.txt").unwrap())
        .lines()
        .map(|line| line.unwrap())
        .filter(|line| !line.is_empty())
        .chunks()
        .map(|lines: [String; 5]| {
            lines.map(|line| {
                line.split(" ")
                    .filter(|elem| !elem.is_empty())
                    .map(|elem| elem.parse::<u32>().unwrap())
                    .take(5)
                    .collect::<Vec<_>>()
            })
        })
        .map(|lines: [Vec<u32>; 5]| Board::from_str_representation(lines))
        .collect::<Vec<Board>>();

    let mut winning_boards = Vec::new();
    for random_number in random_numbers {
        for (board_id, board) in boards.iter_mut().enumerate() {
            //println!("Checking number: {}", random_number);
            board.check_number(random_number);
            if let Some(_) = &board.won {
                //println!("Board with index {} won! Winning score is: {}", board_id, win.score);
                winning_boards.push(board_id);
            }
        }
        if !winning_boards.is_empty() {
            break;
        }
    }

    let mut board_with_highest_winning_score = winning_boards[0];
    let mut highest_score: u32 = boards[winning_boards[0]].won.as_ref().unwrap().score.clone();
    for winning_board in winning_boards {
        let score = boards[winning_board].won.as_ref().unwrap().score.clone();
        if score > highest_score {
            highest_score = score;
            board_with_highest_winning_score = winning_board;
        }
    }

    println!("Part1:");
    println!("Board with index {} won! Winning score is: {}", board_with_highest_winning_score, highest_score);
    println!("Board[{}]: {}", board_with_highest_winning_score, boards[board_with_highest_winning_score])
}
