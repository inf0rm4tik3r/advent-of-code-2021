use std::fs;
use std::io::{BufRead, BufReader};

struct Accumulator {
    x: u32, // horizontal position
    z: u32, // depth, z-axis points downwards!
}

impl Accumulator {
    pub fn new() -> Self {
        Accumulator { x: 0, z: 0 }
    }

    pub fn update_with(&mut self, next_move: Move) {
        match next_move {
            Move::Forward(amount) => self.x += amount,
            Move::Down(amount) => self.z += amount,
            Move::Up(amount) => self.z -= amount,
        };
    }
}

enum Move {
    Forward(u32),
    Down(u32),
    Up(u32),
}

fn parse_move(line: &str) -> Move {
    match line.split_once(" ") {
        Some((move_type_str, amount_str)) => {
            let amount = amount_str.parse::<u32>().unwrap();
            match move_type_str {
                "forward" => Move::Forward(amount),
                "down" => Move::Down(amount),
                "up" => Move::Up(amount),
                _ => panic!("Unknown move command!"),
            }
        }
        _ => panic!("Not a valid move!"),
    }
}

pub fn run() {
    let acc = BufReader::new(fs::File::open("res/input.txt").unwrap())
        .lines()
        .map(|line| line.unwrap())
        .map(|line| parse_move(line.as_str()))
        .fold(Accumulator::new(), |mut acc, next_move| {
            acc.update_with(next_move);
            acc
        });

    println!(
        "Part 1: Final location (horizontal, depth) is ({}, {})! Multiplied: {}",
        acc.x,
        acc.z,
        acc.x * acc.z
    );
}
