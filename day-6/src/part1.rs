use std::fmt::Display;
use std::fs;
use std::io::{BufRead, BufReader};

const INITIAL_DAYS_UNTIL_SPAWN: u32 = 8;
const NEW_DAYS_UNTIL_SPAWN: u32 = 6;

#[derive(Debug, Clone, Copy)]
struct Lanternfish {
    age: u32,
    days_until_spawn: u32,
}

impl Lanternfish {
    pub fn new(days_until_spawn: u32) -> Self {
        Self {
            age: 0,
            days_until_spawn,
        }
    }
}

impl Display for Lanternfish {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.days_until_spawn)
    }
}

#[derive(Debug)]
struct Population<T> {
    population: Vec<T>,
    age: u32,
}

impl<T> Population<T> {
    pub fn size(&self) -> usize {
        self.population.len()
    }
}

trait Simulation {
    fn simulate_day(&mut self);
}

impl Simulation for Population<Lanternfish> {
    fn simulate_day(&mut self) {
        let mut new_fish = Vec::new();
        for fish in &mut self.population {
            if fish.days_until_spawn > 0 {
                fish.days_until_spawn -= 1;
            } else {
                fish.days_until_spawn = NEW_DAYS_UNTIL_SPAWN;
                new_fish.push(Lanternfish::new(INITIAL_DAYS_UNTIL_SPAWN));
            }
            fish.age += 1;
        }
        self.population.append(&mut new_fish);
        self.age += 1;
    }
}

impl<T: Display> Display for Population<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Population at age {} -> {}",
            self.age,
            self.population
                .iter()
                .map(|e| e.to_string())
                .collect::<Vec<_>>()
                .join(", ")
        )
    }
}

pub fn run() {
    let initial_lanternfish = BufReader::new(fs::File::open("res/input.txt").unwrap())
        .lines()
        .map(|line| line.unwrap())
        .flat_map(|line| {
            line.split(",")
                .map(|num| num.parse::<u32>().unwrap())
                .collect::<Vec<_>>()
        })
        .map(|days_until_spawn| Lanternfish::new(days_until_spawn))
        .collect::<Vec<_>>();

    let mut population: Population<Lanternfish> = Population {
        population: initial_lanternfish,
        age: 0,
    };

    println!("Initial population is: {}", population);

    const SIM_DAYS: usize = 80;
    for _ in 0..SIM_DAYS {
        /*let day = i + 1;
        println!(
            "Simulating day {}, Population size {}",
            day,
            population.size()
        );*/
        population.simulate_day();
        /*
        println!(
            "Part1: Population after {} day{} is: {}",
            day,
            if day > 1 { "s" } else { "" },
            population
        );*/
    }

    println!(
        "Part1: Population size after {} days is: {}",
        SIM_DAYS,
        population.size()
    )
}
