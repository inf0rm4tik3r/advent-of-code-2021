use std::fmt::Display;
use std::fs;
use std::io::{BufRead, BufReader};

struct LanternfishPopulation {
    /*
    [
        0 -> amount of fish having 0 days until spawn,
        1 -> amount of fish having 1 days until spawn,
        2 -> amount of fish having 2 days until spawn,
        3 -> amount of fish having 3 days until spawn,
        4 -> amount of fish having 4 days until spawn,
        5 -> amount of fish having 5 days until spawn,
        6 -> amount of fish having 6 days until spawn,
        7 -> amount of fish having 7 days until spawn,
        8 -> amount of fish having 8 days until spawn,
    ]
    */
    population: [u128; 9],
    age: u128,
}

impl LanternfishPopulation {
    pub fn new() -> Self {
        Self {
            population: [0; 9],
            age: 0,
        }
    }

    pub fn size(&self) -> u128 {
        let mut sum = 0;
        for i in 0..9 {
            sum += self.population[i];
        }
        sum
    }
}

impl Display for LanternfishPopulation {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Population at age {}: {}",
            self.age,
            self.population
                .iter()
                .map(|it| it.to_string())
                .collect::<Vec<String>>()
                .join(", ")
        )
    }
}
trait Simulation {
    fn simulate_day(&mut self);
}

impl Simulation for LanternfishPopulation {
    fn simulate_day(&mut self) {
        let zero = self.population[0];

        self.population[0] = self.population[1];
        self.population[1] = self.population[2];
        self.population[2] = self.population[3];
        self.population[3] = self.population[4];
        self.population[4] = self.population[5];
        self.population[5] = self.population[6];
        self.population[6] = self.population[7] + zero; // Each fish at 0 itself is reset!
        self.population[7] = self.population[8];
        self.population[8] = zero; // Each fish at 0 generates a new fish!

        self.age += 1;
    }
}

pub fn run() {
    let mut population = LanternfishPopulation::new();

    BufReader::new(fs::File::open("res/input.txt").unwrap())
        .lines()
        .map(|line| line.unwrap())
        .flat_map(|line| {
            line.split(",")
                .map(|num| num.parse::<u32>().unwrap())
                .collect::<Vec<_>>()
        })
        .for_each(|days_until_spawn| {
            match days_until_spawn {
                0 => population.population[0] += 1,
                1 => population.population[1] += 1,
                2 => population.population[2] += 1,
                3 => population.population[3] += 1,
                4 => population.population[4] += 1,
                5 => population.population[5] += 1,
                6 => population.population[6] += 1,
                7 => population.population[7] += 1,
                8 => population.population[8] += 1,
                _ => panic!("Invalid fish"),
            }
        });

    println!("Initial population is: {}", population);

    const SIM_DAYS: usize = 256;
    for _ in 0..SIM_DAYS {
        /*let day = i + 1;
        println!(
            "Simulating day {}, Population size {}",
            day,
            population.size()
        );*/
        population.simulate_day();
        /*
        println!(
            "Part1: Population after {} day{} is: {}",
            day,
            if day > 1 { "s" } else { "" },
            population
        );*/
    }

    println!(
        "Part2: Population size after {} days is: {}",
        SIM_DAYS,
        population.size()
    )
}
