use std::fs;
use std::io::{BufRead, BufReader};

struct Accumulator {
    increments: u32,
    last: Option<u32>,
}

impl Accumulator {
    pub fn new() -> Self {
        Accumulator {
            increments: 0,
            last: None,
        }
    }

    pub fn update_with(&mut self, measurement: u32) {
        if let Some(last) = self.last {
            if measurement > last {
                self.increments += 1;
            }
        }
        self.last = Some(measurement);
    }
}

pub fn run() {
    let increments = BufReader::new(fs::File::open("res/input.txt").unwrap())
        .lines()
        .map(|line| line.unwrap())
        .map(|line| line.parse::<u32>().unwrap())
        .fold(Accumulator::new(), |mut acc, measurement| {
            acc.update_with(measurement);
            acc
        })
        .increments;

    println!("Part 1: The depth increases {} times!", increments);
}
