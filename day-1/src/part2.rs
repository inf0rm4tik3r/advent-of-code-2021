use std::fs;
use std::io::{BufRead, BufReader};
use std::ops::AddAssign;

use itermore::IterMore;
use num_traits::Num;

trait Sum {
    type Result;
    fn sum(&self) -> Self::Result;
}

impl<T, const SIZE: usize> Sum for [T; SIZE]
where
    T: AddAssign + Num + Copy,
{
    type Result = T;

    fn sum(&self) -> Self::Result {
        let mut sum: T = T::zero();
        for val in self {
            sum += val.clone();
        }
        sum
    }
}

struct Accumulator {
    increments: u32,
    last_sum: Option<u32>,
}

impl Accumulator {
    pub fn new() -> Self {
        Accumulator {
            increments: 0,
            last_sum: None,
        }
    }

    pub fn update_with(&mut self, measurement: [u32; 3]) {
        let sum = measurement.sum();
        if let Some(last_sum) = self.last_sum {
            if sum > last_sum {
                self.increments += 1;
            }
        }
        self.last_sum = Some(sum);
    }
}

pub fn run() {
    let increments = BufReader::new(fs::File::open("res/input.txt").unwrap())
        .lines()
        .map(|line| line.unwrap())
        .map(|line| line.parse::<u32>().unwrap())
        .windows()
        .fold(Accumulator::new(), |mut acc, measurement: [u32; 3]| {
            acc.update_with(measurement);
            acc
        })
        .increments;

    println!("Part 2: The depth increases {} times!", increments);
}
