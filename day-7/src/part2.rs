use num_traits::FromPrimitive;
use std::fs;
use std::io::{BufRead, BufReader};
use std::iter::Sum;
use std::ops::Div;

fn increasing_dist(depth: i32, crab: i32) -> i32 {
    let d = (depth - crab).abs();
    (d * (d + 1)) / 2 // Sum of natural numbers up to d.
}

pub fn run() {
    let crab_positions = BufReader::new(fs::File::open("res/input.txt").unwrap())
        .lines()
        .map(|line| line.unwrap())
        .flat_map(|line| {
            line.split(",")
                .map(|num| num.parse::<i32>().unwrap())
                .collect::<Vec<_>>()
        })
        .collect::<Vec<_>>();

    let result = calc_by_brute_force(crab_positions.clone(), increasing_dist);
    let result2 = calc_fast_using_mean(crab_positions, increasing_dist);

    println!("Part2: SLOW Optimal depth is: {}", result.optimal_depth);
    println!("Part2: SLOW Required fuel is: {}", result.required_fuel);
    println!("Part2: FAST Optimal depth is: {}", result2.optimal_depth);
    println!("Part2: FAST Required fuel is: {}", result2.required_fuel);
}

struct Result {
    optimal_depth: i32,
    required_fuel: i32,
}

fn calc_by_brute_force<F: Fn(i32, i32) -> i32>(crab_positions: Vec<i32>, dist_fn: F) -> Result {
    let min_depth = crab_positions.iter().min().unwrap().clone();
    let max_depth = crab_positions.iter().max().unwrap().clone();
    let mut optimal_depth = i32::MAX;
    let mut required_fuel = i32::MAX;

    for depth in min_depth..=max_depth {
        let mut total_movement_required = 0;
        for crab in &crab_positions {
            total_movement_required += dist_fn(depth, crab.clone()).abs();
        }
        if total_movement_required < required_fuel {
            required_fuel = total_movement_required;
            optimal_depth = depth;
        }
    }
    Result {
        optimal_depth,
        required_fuel,
    }
}

fn calc_fast_using_mean<F: Fn(i32, i32) -> i32>(crab_positions: Vec<i32>, dist_fn: F) -> Result {
    let optimal_depth = mean(&crab_positions);
    let required_fuel = required_fuel_at_depth(optimal_depth, &crab_positions, dist_fn);
    Result {
        optimal_depth,
        required_fuel,
    }
}

fn required_fuel_at_depth<F: Fn(i32, i32) -> i32>(
    depth: i32,
    crab_positions: &[i32],
    dist_fn: F,
) -> i32 {
    crab_positions
        .iter()
        .fold(0, |acc, crab| acc + dist_fn(depth, *crab).abs())
}

fn mean<'a, T: Sum<&'a T> + FromPrimitive + Div<Output = T>>(data: &'a [T]) -> T {
    sum(data) / len(data)
}

fn sum<'a, T: Sum<&'a T>>(data: &'a [T]) -> T {
    data.iter().sum()
}

fn len<T: FromPrimitive>(data: &[T]) -> T {
    FromPrimitive::from_usize(data.len()).unwrap()
}
