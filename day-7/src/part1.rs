use std::fs;
use std::io::{BufRead, BufReader};

use num_traits::Num;

pub fn run() {
    let crab_positions = BufReader::new(fs::File::open("res/input.txt").unwrap())
        .lines()
        .map(|line| line.unwrap())
        .flat_map(|line| {
            line.split(",")
                .map(|num| num.parse::<i32>().unwrap())
                .collect::<Vec<_>>()
        })
        .collect::<Vec<_>>();

    let result = calc_by_brute_force(crab_positions.clone());
    let result2 = calc_fast_using_median(crab_positions);

    println!("Part1: SLOW Optimal depth is: {}", result.optimal_depth);
    println!("Part1: SLOW Required fuel is: {}", result.required_fuel);
    println!("Part1: FAST Optimal depth is: {}", result2.optimal_depth);
    println!("Part1: FAST Required fuel is: {}", result2.required_fuel);
}

struct Result {
    optimal_depth: i32,
    required_fuel: i32,
}

fn calc_by_brute_force(crab_positions: Vec<i32>) -> Result {
    let min_depth = crab_positions.iter().min().unwrap().clone();
    let max_depth = crab_positions.iter().max().unwrap().clone();
    let mut optimal_depth = i32::MAX;
    let mut required_fuel = i32::MAX;

    for depth in min_depth..=max_depth {
        let mut total_movement_required = 0;
        for crab in &crab_positions {
            total_movement_required += (crab - depth).abs();
        }
        if total_movement_required < required_fuel {
            required_fuel = total_movement_required;
            optimal_depth = depth;
        }
    }
    Result {
        optimal_depth,
        required_fuel,
    }
}

fn calc_fast_using_median(mut crab_positions: Vec<i32>) -> Result {
    crab_positions.sort();
    let optimal_depth = median(&crab_positions);
    let required_fuel = crab_positions
        .iter()
        .fold(0, |acc, crab| acc + (crab - optimal_depth).abs());
    Result {
        optimal_depth,
        required_fuel,
    }
}

fn median<T: Num + Copy>(data: &[T]) -> T {
    let len: usize = data.len();
    if len % 2 == 0 {
        let a: T = data[len / 2 - 1];
        let b: T = data[len / 2];
        (a + b) / (T::one() + T::one())
    } else {
        data[len / 2 - 1]
    }
}
