use std::fs;
use std::io::{BufRead, BufReader};

#[derive(Clone, Copy)]
struct Accumulator {
    zeroes: u32,
    ones: u32,
}

impl Accumulator {
    pub fn new() -> Self {
        Accumulator { zeroes: 0, ones: 0 }
    }

    pub fn update_with(&mut self, next_bit: char) {
        match next_bit {
            '0' => self.zeroes += 1,
            '1' => self.ones += 1,
            _ => panic!("Invalid input"),
        };
    }

    pub fn equal_occurrences(&self) -> bool {
        self.zeroes == self.ones
    }

    pub fn most_common_bit(&self) -> char {
        if self.ones >= self.zeroes {
            return '1';
        }
        return '0';
    }

    pub fn least_common_bit(&self) -> char {
        if self.ones > self.zeroes {
            return '0';
        }
        return '1';
    }
}

// Use 5 for test input and 12 for real input.
const LINE_LENGTH: usize = 12;

fn extract_data<F: Fn(Accumulator) -> char>(lines: &Vec<String>, desired: F, default: char) -> String {
    let mut selected = lines.clone();
    for i in 0..LINE_LENGTH {
        let mut accumulator = Accumulator::new();
        selected.iter().for_each(|line| {
            let c = line.chars().skip(i).next().unwrap();
            accumulator.update_with(c);
        });
        if selected.len() > 1 {
            selected = selected
                .into_iter()
                .filter(|line| {
                    let c = line.chars().into_iter().skip(i).next().unwrap();
                    let desired_char = match accumulator.equal_occurrences() {
                        true => default,
                        false => desired(accumulator),
                    };
                    if c != desired_char {
                        return false;
                    }
                    true
                })
                .collect::<Vec<String>>();
            }
    }
    selected.first().unwrap().clone()
}

pub fn run() {

    let lines = BufReader::new(fs::File::open("res/input.txt").unwrap())
        .lines()
        .map(|line| line.unwrap())
        .collect::<Vec<String>>();

    let oxygen_generator_rating_string = extract_data(&lines, |acc| acc.most_common_bit(), '1');
    let co2_scrubber_rating_string = extract_data(&lines, |acc| acc.least_common_bit(), '0');

    println!("oxygen_generator_rating_string: {}", oxygen_generator_rating_string);
    println!("co2_scrubber_rating_string: {}", co2_scrubber_rating_string);

    let oxygen_generator_rating = u32::from_str_radix(oxygen_generator_rating_string.as_str(), 2).unwrap();
    let co2_scrubber_rating = u32::from_str_radix(co2_scrubber_rating_string.as_str(), 2).unwrap();

    println!(
        "Part 2: oxygen generator is {}, co2 scrubber is {}! Lief support is: {}",
        oxygen_generator_rating,
        co2_scrubber_rating,
        oxygen_generator_rating * co2_scrubber_rating
    );
}
