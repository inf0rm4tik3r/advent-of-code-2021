use std::fs;
use std::io::{BufRead, BufReader};

#[derive(Clone, Copy)]
struct Accumulator {
    zeroes: u32,
    ones: u32,
}

impl Accumulator {
    pub fn new() -> Self {
        Accumulator { zeroes: 0, ones: 0 }
    }

    pub fn update_with(&mut self, next_bit: char) {
        match next_bit {
            '0' => self.zeroes += 1,
            '1' => self.ones += 1,
            _ => panic!("Invalid input"),
        };
    }

    pub fn most_common_bit(&self) -> char {
        if self.ones > self.zeroes {
            return '1';
        }
        return '0';
    }

    pub fn least_common_bit(&self) -> char {
        if self.ones > self.zeroes {
            return '0';
        }
        return '1';
    }
}

pub fn run() {
    // Use 5 for test input and 12 for real input.
    let mut accumulators = [Accumulator::new(); 12];

    BufReader::new(fs::File::open("res/input.txt").unwrap())
        .lines()
        .map(|line| line.unwrap())
        .for_each(|line| {
            line.chars().enumerate().for_each(|(i, c)| accumulators[i].update_with(c))
        });

    let mut gamma_bit_string = String::new();
    for acc in &accumulators {
        gamma_bit_string.push(acc.most_common_bit());
    }
    println!("Gamma as bit string: {}", gamma_bit_string);

    let mut epsilon_bit_string = String::new();
    for acc in &accumulators {
        epsilon_bit_string.push(acc.least_common_bit());
    }
    println!("Epsilon as bit string: {}", epsilon_bit_string);

    let gamma = u32::from_str_radix(gamma_bit_string.as_str(), 2).unwrap();
    let epsilon = u32::from_str_radix(epsilon_bit_string.as_str(), 2).unwrap();

    println!(
        "Part 1: gamma is {}, epsilon is {}! Power is: {}",
        gamma,
        epsilon,
        gamma * epsilon
    );
}
